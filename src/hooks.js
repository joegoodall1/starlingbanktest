import { useState, useEffect } from 'react';

function useFetch(url) {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);

  async function fetchData() {
    const response = await fetch(url, {
      method: 'GET',
      headers: new Headers({
        Accept: 'application/json',
        Authorization:
          'Bearer gTVyPXcvmortMqDq96yruiggcwJo7q18T6Zb8dJzn0c2h5JmpGE2qM3U2uzdVWFY'
      })
    });
    const json = await response.json();
    setData(json);
    setLoading(false);
  }
  useEffect(() => {
    fetchData();
  }, []);

  return { loading, data };
}

export { useFetch };
