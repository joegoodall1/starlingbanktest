// @flow

import React from 'react';
import { useFetch } from './hooks';
import styled from 'styled-components';
import Spinner from 'react-spinkit';
import img from './assets/beach.jpg';

const HeaderContent = styled.div`
  background-image: url(${img});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  width: 100%;
  height: 200px;
`;

const TextContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Row = styled.div`
  display: flex;
  justify-content: space-around;
  width: 50%;
  margin: 7px;
`;

const TransactionRow = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 7px;
  box-shadow: 1px 1px 1px 1px lightgray;
  background-color: white;
  border-radius: 5px;
`;

const Container = styled.div`
  height: 325px;
  overflow-y: scroll
  background-color: lightgray;
  width: 50%;
`;

const SpinnerContainer = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const HeaderText = styled.h1`
  text-align: center;
  position: absolute;
  text-shadow: 0 0 5px black;
  top: 20%;
  left: 50%;
  font-size: 5em;
  transform: translate(-50%, -50%);
  color: white;
`;

const Button = styled.button`
  height: 50px;
  border-radius: 50px;
  font-size: 1em;
  border: 2px solid lightblue;
  color: lightblue;
`;

function App() {
  const { loading, data } = useFetch(
    '/v1/transactions?from=2019-03-06&to2019-03-08'
  );

  const addRows = () => {
    if (!loading) {
      return data._embedded.transactions
        .filter(item => item.direction === 'OUTBOUND')
        .map(item => (
          <TransactionRow>
            <h3>{Math.abs(item.amount).toFixed(2)}</h3>
            <h3>{(1 - (Math.ceil(item.amount) - item.amount)).toFixed(2)}</h3>
          </TransactionRow>
        ));
    }
  };

  const sumFunction = () => {
    if (!loading) {
      return data._embedded.transactions
        .filter(item => item.direction === 'OUTBOUND')
        .map(item => 1 - (Math.ceil(item.amount) - item.amount))
        .reduce((a, b) => a + b, 0)
        .toFixed(2);
    }
  };
  const sumTotal = sumFunction();

  const authHeaders = new Headers({
    Accept: 'application/json',
    Authorization:
      'Bearer gTVyPXcvmortMqDq96yruiggcwJo7q18T6Zb8dJzn0c2h5JmpGE2qM3U2uzdVWFY',
    'Content-Type': 'application/json'
  });

  const payload = {
    name: 'Goals',
    currency: 'GBP',
    target: {
      currency: 'GBP',
      minorUnits: sumTotal
    },
    base64EncodedPhoto: ''
  };

  const putTotal = () => {
    fetch('/v2/account/6ac3a858-cf45-f63f-086e-bb05e2ffbe00/savings-goals', {
      method: 'PUT',
      headers: authHeaders,
      body: JSON.stringify(payload)
    })
      .then(res => res.json())
      .then(response => console.log('Success:', JSON.stringify(response)))
      .catch(error => console.error('Error:', error));
  };

  return (
    <>
      <HeaderContent>
        <HeaderText>Goals</HeaderText>
      </HeaderContent>
      {loading ? (
        <SpinnerContainer>
          <Spinner name="ball-spin-fade-loader" />
        </SpinnerContainer>
      ) : (
        <>
          <TextContent>
            <Row>
              <h2>Spent (£)</h2>
              <h2>Add (£)</h2>
            </Row>
            <Container>{addRows()}</Container>
            <Row>
              <h1 />
              <h2>Total: £{sumTotal}</h2>
            </Row>
            <Button onClick={putTotal}> ADD MONEY </Button>
          </TextContent>
        </>
      )}
    </>
  );
}

export default App;
