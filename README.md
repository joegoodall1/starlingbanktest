
## Available Scripts

In the project directory, you can run:

### `yarn`

to install node modules followed by

### `yarn start`

which runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## ToDo

I had problems with the PUT endpoints and couldn't get it to work in the allotted time. It worked fine with Postman so not sure what was going wrong.

If I'd have had more time I would have used [big.js](https://github.com/MikeMcl/big.js/).


